﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace NetGroup_ConsoleApp
{
    class Program
    {

        private const string COMMAND_MOVE_UP = "üles";
        private const string COMMAND_MOVE_DOWN = "alla";

        static void Main(string[] args)
        {
            

                var yourFloor = new Random().Next(1, 13); //generator.RandomNumber(1, 13);
                Console.WriteLine($"Sinu asud {yourFloor}. korrusel!");
                IList<Elevator> elevators = new List<Elevator>();
                var elevator = new Elevator(yourFloor, new Random().Next(1, 13));
                var elevator1 = new Elevator(yourFloor, new Random().Next(1, 13));
                var elevator2 = new Elevator(yourFloor, new Random().Next(1, 13));
                elevators.Add(elevator);
                elevators.Add(elevator1);
                elevators.Add(elevator2);

                Elevator closest = null;
                foreach (var elev in elevators)
                {
                    if (closest == null)
                    {
                        closest = elev;
                    }
                    else
                    {
                        var diff = Math.Abs(yourFloor - elev.ElevatorFloor);
                        var currDiff = Math.Abs(yourFloor - closest.ElevatorFloor);
                        if (diff < currDiff)
                            closest = elev;
                    }
                    Console.WriteLine($"Saadaval on lift {elev.ElevatorFloor}. korrusel.");
                }

                if (yourFloor != closest.ElevatorFloor)
                {
                    switch (yourFloor)
                    {

                        case 1:
                            {
                                Console.WriteLine("Saad sõita ainult üles! Kirjuta konsooli 'üles' ");
                                if (Console.ReadLine().ToString() == COMMAND_MOVE_UP)
                                {
                                    Console.WriteLine($"Sinu lift asub {closest.ElevatorFloor}. korrusel!");
                                    closest.Call();
                                    closest.Move();
                                }
                                else
                                    Console.WriteLine("Sa saad liikuda ainult üles!");

                                break;
                            }
                        case 13:
                            {
                                Console.WriteLine("Saad sõita ainult alla! Kirjuta konsooli 'alla'");
                                if (Console.ReadLine().ToString() == COMMAND_MOVE_DOWN)
                                {
                                    Console.WriteLine($"Sinu lift asub {closest.ElevatorFloor}. korrusel!");
                                    closest.Call();
                                    closest.Move();
                                }
                                else
                                    Console.WriteLine("Sa saad liikuda ainult alla!");
                                break;
                            }

                        default:
                            {

                                Console.WriteLine("Kas soovid sõita üles või alla?");
                                string commandInput = Console.ReadLine();
                                if (commandInput == COMMAND_MOVE_UP)
                                {
                                    Console.WriteLine($"Sinu lift asub {closest.ElevatorFloor}. korrusel!");
                                    closest.Call();
                                    closest.Move();
                                }
                                else if (commandInput == COMMAND_MOVE_DOWN)
                                {
                                    Console.WriteLine($"Sinu lift asub {closest.ElevatorFloor}. korrusel!");
                                    closest.Call();
                                    closest.Move();
                                }
                                else
                                {
                                    Console.WriteLine("Sa saad liikuda ainult üles või alla!");
                                }

                                break;
                            }
                    }
                }
                else
                {

                    Console.WriteLine("Kas soovid sõita üles või alla?");
                    string commandInput = Console.ReadLine();
                    if (commandInput == COMMAND_MOVE_UP)
                    {
                        Console.WriteLine($"Sinu lift asub {closest.ElevatorFloor}. korrusel!");
                        closest.elevatorStopped();
                        closest.Move();
                    }
                    else if (commandInput == COMMAND_MOVE_DOWN)
                    {
                        Console.WriteLine($"Sinu lift asub {closest.ElevatorFloor}. korrusel!");
                        closest.elevatorStopped();
                        closest.Move();
                    }
                    else
                    {
                        Console.WriteLine("Sa saad liikuda ainult üles või alla!");
                    }

                }

            
            }

        }
    }
