﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace NetGroup_ConsoleApp
{
    public class Elevator
    {
        private const string COMMAND_MOVE_UP = "üles";
        private const string COMMAND_MOVE_DOWN = "alla";

        private int currentFloor;

        public int ElevatorFloor { get; private set; }

        public Elevator(int startFloor, int elevatorStartFloor)
        {
            currentFloor = startFloor;
            ElevatorFloor = elevatorStartFloor;
        }


        public void Call()
        {

            floorInput(currentFloor);
            elevatorStopped();
        }

        public void Move()
        {
            ElevatorFloor = currentFloor;
            Console.WriteLine("Sisestage korrus!");
            var readInput = Convert.ToInt32(Console.ReadLine());
            do
            {
              Console.WriteLine("Sa oled juba sellel korrusel!");
              readInput = Convert.ToInt32(Console.ReadLine());
                
            } while (readInput==ElevatorFloor);
            floorInput(readInput);
            Stop();
        }

        public void Stop()
        {
            Console.WriteLine("Lift on kohal!Uksed avanevad...Kõike Head!");
        }
        void elevatorUP(int destinationFloor)
        {
            Console.WriteLine("Lift liigub üles...");
            for (int i = ElevatorFloor; i <= destinationFloor; i++)
            {

                Thread.Sleep(2000);
                Console.WriteLine("{0}", i);

            }
        }
        void elevatorDown(int destinationFloor)
        {
            Console.WriteLine("Lift liigub alla...");
            for (int i = ElevatorFloor; i >= destinationFloor; i--)
            {

                Thread.Sleep(2000);
                //Task.Delay(2000); //use with async only
                Console.WriteLine("{0}", i);

            }
        }
        public void elevatorStopped()
        {
            Console.WriteLine("Lift on kohal!Uksed avanevad...");
            Thread.Sleep(2000);
            Console.WriteLine("Astuge lifti!");

        }
        void floorInput(int input)
        {
            

                if (input == ElevatorFloor)
                    Console.WriteLine("Sa oled juba sellel korrusel!");
                else if (input > ElevatorFloor)
                    elevatorUP(input);
                else if (input < ElevatorFloor)
                    elevatorDown(input);
                else
                    Console.WriteLine("error");
           
        }
    }
}
